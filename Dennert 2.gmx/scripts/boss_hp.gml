if room = Magellan_Boss
  {
  draw_set_color(c_gray);
  draw_roundrect(view_xview[0] + 320 - 250, view_yview[0] + 460, view_xview[0] + 320 + 250, view_yview[0] + 475, false);
  draw_set_color(c_lime);
  draw_roundrect(view_xview[0] + 320 - hp * 2.5, view_yview[0] + 460, view_xview[0] + 320 + hp * 2.5, view_yview[0] + 475, false);
  draw_set_color(c_black);
  draw_roundrect(view_xview[0] + 320 - 250, view_yview[0] + 460, view_xview[0] + 320 + 250, view_yview[0] + 475, true);
  }
if room = Khan_Boss
  {
  draw_set_color(c_gray);
  draw_roundrect(view_xview[0] + 320 - 250, view_yview[0] + 460, view_xview[0] + 320 + 250, view_yview[0] + 475, false);
  draw_set_color(c_lime);
  draw_roundrect(view_xview[0] + 320 - hp * 1.25, view_yview[0] + 460, view_xview[0] + 320 + hp * 1.25, view_yview[0] + 475, false);
  draw_set_color(c_black);
  draw_roundrect(view_xview[0] + 320 - 250, view_yview[0] + 460, view_xview[0] + 320 + 250, view_yview[0] + 475, true);
  }
if room = Musashi_Boss
  {
  draw_set_color(c_gray);
  draw_roundrect(view_xview[0] + 320 - 250, view_yview[0] + 460, view_xview[0] + 320 + 250, view_yview[0] + 475, false);
  draw_set_color(c_lime);
  draw_roundrect(view_xview[0] + 320 - hp * 3.125, view_yview[0] + 460, view_xview[0] + 320 + hp * 3.125, view_yview[0] + 475, false);
  draw_set_color(c_black);
  draw_roundrect(view_xview[0] + 320 - 250, view_yview[0] + 460, view_xview[0] + 320 + 250, view_yview[0] + 475, true);
  }
if room = Mecha_Musashi
  {
  draw_set_color(c_orange);
  draw_roundrect(view_xview[0] + 320 - 250, view_yview[0] + 460, view_xview[0] + 320 + 250, view_yview[0] + 475, false);
  draw_set_color(c_red);
  draw_roundrect(view_xview[0] + 320 - hp / 4, view_yview[0] + 460, view_xview[0] + 320 + hp / 4, view_yview[0] + 475, false);
  draw_set_color(c_black);
  draw_roundrect(view_xview[0] + 320 - 250, view_yview[0] + 460, view_xview[0] + 320 + 250, view_yview[0] + 475, true);
  }
