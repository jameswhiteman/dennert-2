// NOTE: the 'sup' in the script name stands for superfluous,
// as in this script does nothing for the game and is only
// for organizational purposes.

/*

After organizing the Teacher's Union, Dennert informs the group that College Board
is up to its old tricks again. Dennert's inside man at College Board has uncovered
plans for releasing a new AP Government Exam this year that automatically turns
on the phones of students using new test-failing technology. It's up to the
Teacher's Union- comprised of Brian Dennert, Dan Shuster, Lisa McCabe, and
Jamie Snodgrass- to thwart these plans. They must journey to College Board's
headquarters and destroy this AP test before it wastes the $87 of millions of
helpless students.

The Union battles through Royal High School, then through the streets of Simi Valley,
then through the underground caverns of the city, then they reach the city's emergency
spaceship. They take it into space and fight their way into College Board Headquarters,
where they fight the President of College Board, Gaston Caperton, who then reveals that
the new AP test doesn't turn on student's cell phones anymore- that was the old model!
The new model destroys students- literally. Out comes the giant, murderous AP test,
which the Teacher's Union feels strongly intimidated by. Jeffrey the Unicorn comes in
the nick of time, bringing along Stanley, Maxwell, and Theodore the Unicorns, and
together the Teacher's Union brings down the AP test and flies off into the sunset.

*/
