fname = get_open_filename("Dennert Saves|*.dnrt", "");
global.checkpoint = false;

if file_exists(fname)
  {
  file = file_text_open_read(fname);
  global.up = file_text_read_real(file); file_text_readln(file);//global.up
  global.left = file_text_read_real(file); file_text_readln(file);//global.left
  global.right = file_text_read_real(file); file_text_readln(file);//global.right
  global.kick = file_text_read_real(file); file_text_readln(file);//global.kick
  global.secrets = file_text_read_string(file); file_text_readln(file);//secrets database
  global.sliding = file_text_read_string(file); file_text_readln(file);
  global.spartankickshockwave = file_text_read_string(file); file_text_readln(file);
  global.homingattack = file_text_read_string(file); file_text_readln(file);
  global.flying = file_text_read_string(file); file_text_readln(file);
  global.unihorngun = file_text_read_string(file); file_text_readln(file);
  global.rapidfire = file_text_read_string(file); file_text_readln(file);
  global.strongbullets = file_text_read_string(file); file_text_readln(file);
  global.superunihorns = file_text_read_string(file); file_text_readln(file);
  global.battleunicorn = file_text_read_string(file); file_text_readln(file);
  //global.screenshake = file_text_read_real(file); file_text_readln(file);//global.screenshake
  global.gamebeaten = file_text_read_real(file); file_text_readln(file);//global.gamebeaten
  global.fullscreenoption = file_text_read_real(file); file_text_readln(file);//global.fullscreenoption
  global.musicoption = file_text_read_real(file); file_text_readln(file);//global.musicoption
  global.soundoption = file_text_read_real(file); file_text_readln(file);//global.soundoption
  room = file_text_read_real(file); file_text_readln(file);//room
  global.savegame = file_text_read_string(file); file_text_readln(file);//name of save file
  global.fsas = file_text_read_real(file); file_text_readln(file);//fine silks and scarves
  global.silksscarves = file_text_read_string(file); file_text_readln(file);//fine silks and scarves database
  
  file_text_close(file);
  show_message("Game load successful.");
  }
