global.checkpoint = false;
file_delete(string(global.savegame) + ".sav");
file = file_text_open_write(string(global.savegame) + ".sav");

file_text_write_real(file, global.up); file_text_writeln(file);
file_text_write_real(file, global.left); file_text_writeln(file);
file_text_write_real(file, global.right); file_text_writeln(file);
file_text_write_real(file, global.kick); file_text_writeln(file);
file_text_write_string(file, ds_list_write(global.secrets)); file_text_writeln(file);
/*
if ds_list_find_index(global.secrets, sec_sliding)
{file_text_write_string(file, global.sliding); file_text_writeln(file);}
else
{file_text_write_string(file, "Off"); file_text_writeln(file);}
if ds_list_find_index(global.secrets, sec_spartankickshockwave)
{file_text_write_string(file, global.spartankickshockwave); file_text_writeln(file);}
else
{file_text_write_string(file, "Off"); file_text_writeln(file);}
if ds_list_find_index(global.secrets, sec_homingattack)
{file_text_write_string(file, global.homingattack); file_text_writeln(file);}
else
{file_text_write_string(file, "Off"); file_text_writeln(file);}
if ds_list_find_index(global.secrets, sec_flying)
{file_text_write_string(file, global.flying); file_text_writeln(file);}
else
{file_text_write_string(file, "Off"); file_text_writeln(file);}
if ds_list_find_index(global.secrets, sec_unihorngun)
{file_text_write_string(file, global.unihorngun); file_text_writeln(file);}
else
{file_text_write_string(file, "Off"); file_text_writeln(file);}
if ds_list_find_index(global.secrets, sec_rapidfire)
{file_text_write_string(file, global.rapidfire); file_text_writeln(file);}
else
{file_text_write_string(file, "Off"); file_text_writeln(file);}
if ds_list_find_index(global.secrets, sec_strongbullets)
{file_text_write_string(file, global.strongbullets); file_text_writeln(file);}
else
{file_text_write_string(file, "Off"); file_text_writeln(file);}
if ds_list_find_index(global.secrets, sec_superunihorns)
{file_text_write_string(file, global.superunihorns); file_text_writeln(file);}
else
{file_text_write_string(file, "Off"); file_text_writeln(file);}
if ds_list_find_index(global.secrets, sec_battleunicorn)
{file_text_write_string(file, global.battleunicorn); file_text_writeln(file);}
else
{file_text_write_string(file, "Off"); file_text_writeln(file);}

file_text_write_real(file, global.screenshake); file_text_writeln(file);
*/
file_text_write_real(file, global.gamebeaten); file_text_writeln(file);
file_text_write_real(file, global.fullscreenoption); file_text_writeln(file);
file_text_write_real(file, global.musicoption); file_text_writeln(file);
file_text_write_real(file, global.soundoption); file_text_writeln(file);
file_text_write_real(file, room); file_text_writeln(file);
file_text_write_string(file, global.savegame); file_text_writeln(file);
file_text_write_real(file, global.fsas); file_text_writeln(file);
file_text_write_string(file, ds_list_write(global.silksscarves));

file_text_close(file);
