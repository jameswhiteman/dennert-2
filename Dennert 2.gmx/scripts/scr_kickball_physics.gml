// Kickballs sitting on top of each other and moving off
if place_free(x, y + 1) && vspeed = 0 && place_meeting(x, y + 1, kickball)
  {
  if ball.x <= x
  hspeed = 1;
  else
  hspeed = -1;
  }

// Kickball gravity
if place_free(x, y + vspeed) && place_free(x, y + 1) && !place_meeting(x + hspeed, y + vspeed + 1, kickball)
  {
  if place_meeting(x, y, wash_water_1)
  gravity = 0.1;
  else
  gravity = 0.3;
  }
else if current_gravity = 0
gravity = 0;
else
  {
  if !place_meeting(x, y, kickball)
  gravity = 0.5;
  else
    {
    ball = instance_place(x, y, kickball)
    gravity = 0;
    if abs(ball.x - x) <= abs(ball.y - y)
    for (i = 0; i < 32; i += 1)
      {
      if ball.x > x
        {
        if !place_meeting(x - i, y, kickball)
          {
          x -= i;
          break;
          }
        }
      else
        {
        if !place_meeting(x + i, y, kickball)
          {
          x += i;
          break;
          }
        }
      }
    else
    for (i = 0; i < 32; i += 1)
      {
      if ball.y > y
        {
        if !place_meeting(x, y - i, kickball)
          {
          y -= i;
          break;
          }
        }
      else
        {
        if !place_meeting(x, y + i, kickball)
          {
          y += i;
          break;
          }
        }
      }
    }
  }

// Kickballs bouncing off of each other
if place_meeting(x + hspeed, y + vspeed, kickball) && current_gravity = 0
  {
  ball = instance_place(x + hspeed, y + vspeed, kickball);
  //hspeed
  if ball.hspeed > 0 && hspeed > 0 && hspeed > ball.hspeed
    {
    ball.hspeed = hspeed;
    hspeed = hspeed * 0.85;
    }
  if ball.hspeed < 0 && hspeed < 0 && hspeed < ball.hspeed
    {
    ball.hspeed = hspeed;
    hspeed = hspeed * 0.85;
    }
  if (ball.hspeed < 0 && hspeed > 0) || (ball.hspeed > 0 && hspeed < 0)
    {
    hspeed *= -0.85;
    ball.hspeed *= -0.85;
    }
  if ball.hspeed = 0 && hspeed != 0
    {
    ball.hspeed += hspeed * 0.85;
    hspeed *= -0.85;
    
    }
    //vspeed
  if ball.vspeed > 0 && vspeed > 0 && vspeed > ball.vspeed
    {
    ball.vspeed = vspeed;
    vspeed = vspeed * 0.85;
    }
  if ball.vspeed < 0 && vspeed < 0 && vspeed < ball.vspeed
    {
    ball.vspeed = vspeed;
    vspeed = vspeed * 0.85;
    }
  if (ball.vspeed < 0 && vspeed > 0) || (ball.vspeed > 0 && vspeed < 0)
    {
    vspeed *= -0.85;
    ball.vspeed *= -0.85;
    }
  if ball.vspeed = 0 && vspeed != 0
    {
    if x >= ball.x
      {
      hspeed += vspeed * 0.85;
      ball.hspeed -= vspeed * 0.85;
      }
    else
      {
      hspeed -= vspeed * 0.85;
      ball.hspeed += vspeed * 0.85;
      }
    vspeed *= -0.85;
    }
  }
  
// If being attracted to a SOAPSTone and they collide with each other, bounce
if current_gravity = 1 && place_meeting(x + hspeed, y + vspeed, kickball)
move_bounce_all(true);
